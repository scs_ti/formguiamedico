<?php include 'html/header.html'; ?>

    <div class="d-flex align-items-center justify-content-around my-5 home_guia">
        <div class="row">
            <div class="col-md-4 mb-2">
                <a href="beneficiario.php" class="d-flex flex-column align-items-center">
                    <!--<img src="img/ben.png" alt="" class="mb-2">-->
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <h3>Sou beneficiario</h3>
                </a>
            </div>
            
            <div class="col-md-4 mb-2">
                <a href="naoBeneficiario.php" class="d-flex flex-column align-items-center">
                    <!--<img src="img/nBen.png" alt="" class="mb-2">-->
                    <i class="fa fa-user-times" aria-hidden="true"></i>
                    <h3>Não sou beneficiario</h3>
                </a>
            </div>

            <div class="col-md-4">
                <a href="https://www.planosantacasasaude.com/beneficiario/guia-medico/" class="d-flex flex-column align-items-center">
                    <!--<img src="img/nBen.png" alt="" class="mb-2">-->
                    <i class="fa fa-download" aria-hidden="true"></i>
                    <h3>Guia Médico para Download</h3>
                </a>
            </div>
        </div>
    </div>
</div>
<?php include 'html/footer.html';?>

