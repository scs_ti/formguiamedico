<?php 

    include 'connect.php';  

    $conexao = DBOps::connect();    

    $sql = 'SELECT DISTINCT INITCAP("MedicoLocalAtendimento") AS "MedicoLocalAtendimento" FROM V_GUIA_MEDICO_APP';

    $std = oci_parse($conexao, $sql);
    oci_execute($std);
    oci_fetch_all($std, $localAtendimento, 0, -1, OCI_ASSOC);
    
    echo json_encode($localAtendimento);

?>