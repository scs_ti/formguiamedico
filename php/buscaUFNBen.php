<?PHP

    include 'connect.php';     
    
    $conexao = DBOps::connect();

    $stt = 'SELECT DISTINCT UPPER(INITCAP(vgma.UF)) AS UF FROM V_GUIA_MEDICO_APP vgma';

    $std = oci_parse($conexao, $stt);
    oci_execute($std);
    oci_fetch_all($std, $municipios, 0, -1, OCI_ASSOC);
    
    echo json_encode($municipios);

?>