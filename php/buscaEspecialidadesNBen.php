<?PHP

    include 'connect.php';     
    
    $conexao = DBOps::connect();
    $plano = $_POST['plano'];

    // $stt = 'SELECT DISTINCT INITCAP(mvep.ESPECIALIDADE) AS ESPECIALIDADE 
    //         FROM MV_ESPECIALIDADES_PLANO mvep
    //         WHERE mvep."DS_PLANO" = ' . "'" . $plano . "'
    //         ORDER BY ESPECIALIDADE";

    $stt = 'SELECT DISTINCT INITCAP(mvep.ESPECIALIDADE) AS ESPECIALIDADE 
            FROM MV_ESPECIALIDADES_PLANO mvep
            ORDER BY ESPECIALIDADE';

    $std = oci_parse($conexao, $stt);
    oci_execute($std);
    oci_fetch_all($std, $especialidades);
    
    echo json_encode($especialidades);

?>