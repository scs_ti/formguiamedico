<?PHP

    include 'connect.php';     
    
    $conexao = DBOps::connect();

    $stt = 'SELECT DISTINCT INITCAP("DS_PLANO") AS "Classificao_Guia", INITCAP("TIPO_CONTRATO") AS "TIPO_CONTRATO", RG_ANS FROM MV_ESPECIALIDADES_PLANO  me INNER JOIN planos_guia_medico pg ON pg.COD_PLANO = me.CD_PLANO 
    ORDER BY "Classificao_Guia"';
    // $stt = 'SELECT DISTINCT INITCAP("Classificao_Guia") AS "Classificao_Guia" FROM MV_ESPECIALIDADES_PLANO ORDER BY "Classificao_Guia"';

    $std = oci_parse($conexao, $stt);
    oci_execute($std);
    oci_fetch_all($std, $planos, 1, -1);
    
    echo json_encode($planos);

?>