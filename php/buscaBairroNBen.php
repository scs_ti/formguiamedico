<?PHP

    include 'connect.php';     
    
    $conexao = DBOps::connect();
    $cidade = $_POST['cidade'];

    $stt = "SELECT DISTINCT INITCAP(vgma.BAIRRO) AS bairro FROM V_GUIA_MEDICO_APP vgma WHERE CIDADE = UPPER('$cidade')";

    $std = oci_parse($conexao, $stt);
    oci_execute($std);
    oci_fetch_all($std, $bairro, 0, -1, OCI_ASSOC);
    
    echo json_encode($bairro);

?>