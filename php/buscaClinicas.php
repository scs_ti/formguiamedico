<?PHP

    include 'connect.php';     
    
    $conexao = DBOps::connect();
    $where = $_POST['where'];

    $stt = 'SELECT DISTINCT 
            MEDICOID, 
            "MedicoCRM",
            gmta."Titulo Cod" AS CERTIFICACAO,
            INITCAP(MEDICONOME) AS MEDICONOME,
            INITCAP("Logradouro") AS "Logradouro", 
            "Numero",
            vgma.UF,
            INITCAP(vgma.CIDADE) AS CIDADE,
            INITCAP(BAIRRO) AS BAIRRO, 
            vgma.CEP, 
            "Telefone" ,
            P.SN_ISO_9001,
            vgma.CNPJ,
            P.SN_MESTRADO,
            P.SN_EXCLUSIVO_ODONTO,
            P.SN_RESIDENCIA,
            INITCAP(vgma.ESPECIALIDADE) AS ESPECIALIDADE,
            "Complemento",
            "MedicoLocalAtendimento",
            TIPO
            FROM dbaps.v_guia_medico_app vgma
            FULL OUTER JOIN dbaps.v_guia_medico_titulos_app gmta ON gmta."Medico ID" = vgma.MEDICOID
            INNER JOIN PRESTADOR P ON P.CD_PRESTADOR = vgma.MEDICOID
            LEFT JOIN base_dados_guia_medico bd ON bd.CNPJ_CPF = vgma.CNPJ 
            WHERE ' . $where . ' ORDER BY MEDICONOME';

    $std = oci_parse($conexao, $stt);
    oci_execute($std);
    oci_fetch_all($std, $medicos, 0, -1, OCI_FETCHSTATEMENT_BY_ROW | OCI_ASSOC);
    
    echo json_encode($medicos);

    
?>