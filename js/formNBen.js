function carregaPlanos() {
    $.ajax({
        url: "php/buscaPlanos.php",
        method: 'Post',
        success: function (planos) {
            planos = JSON.parse(planos)
            $('select#plano').html('<option disabled selected>Selecione</option>')
            for (var i = 0; i < planos['Classificao_Guia'].length; i++) {
                var newPlano = planos['Classificao_Guia'][i] + ' - ' + planos['TIPO_CONTRATO'][i] + ' - ' + planos['RG_ANS'][i];
                $('select#plano').append('<option>' + newPlano + '</option>')
            }
        }
    })
}

function carregaLocAtendimento(){
    $.ajax({
        url: "php/tipoLocalAtendimento.php",
        method: 'Post',
        success: function (tipoLocalAtendimento) {
            tipoLocalAtendimento = JSON.parse(tipoLocalAtendimento)
            $('select#estabelecimento').html('<option disabled selected>Selecione</option>')
            tipoLocalAtendimento['MedicoLocalAtendimento'].forEach(tipoLocalAtendimento => {
                $('select#estabelecimento').append('<option>' + tipoLocalAtendimento + '</option>')
            })
        }
    })
}

function carregaUF() {
    $.ajax({
        url: "php/buscaUFNBen.php",
        method: 'Post',
        success: function (ufs) {
            ufs = JSON.parse(ufs)
            $('select#UF').html('<option disabled selected>Selecione</option>')
            ufs['UF'].forEach(ufs => {
                $('select#UF').append('<option>' + ufs + '</option>')
            })
        }
    })
}

function carregaEspecialidadesNBen(plano) {
    $('#cdPlano').attr('value', plano)

    $('#erroIdentificacao').attr('hidden', 'true')
    $('#carregandoEspecialidades').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')

    $('#especialidades').attr('disabled', 'true')
    $('#especialidades').html("<option disabled selected>Selecione</option>")

    $('#gMedicos').attr('hidden', 'true')

    $.ajax({
        url: "php/buscaEspecialidadesNBen.php",
        method: 'POST',
        data: { plano: plano.split(' - ')[0] },
        success: function (especialidades) {
            especialidades = JSON.parse(especialidades)
            $('#especialidades').removeAttr('disabled')
            $('#carregandoEspecialidades').attr('hidden', 'true')
            $('#buscaRealizada').removeAttr('hidden')
            especialidades['ESPECIALIDADE'].forEach(especialidade => {
                $('#especialidades').append("<option>" + especialidade + "</option>")
            });

            setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)

        }
    })
}

function buscaMunicipiosNBen(uf) {
    $('#carregandoMunicipios').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')

    $('#gMedicos').attr('hidden', 'true')
    $.ajax({
        url: "php/buscaMunicipiosNBen.php",
        method: 'Post',
        data: {
            uf: uf,
        },
        success: function (municipios) {
            municipios = JSON.parse(municipios)
            $('#municipios').html("<option disabled selected>Selecione</option>")
            municipios['CIDADE'].forEach(municipio => {
                $('#municipios').append("<option>" + municipio + "</option>")
            });
            $('#municipios').removeAttr('disabled')
            $('#carregandoMunicipios').attr('hidden', true)
            $('#buscaRealizada').removeAttr('hidden')
            setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)
        }
    })
}

function buscaBairro(cidade) {
    $('#bairros').val('Selecione')
    $('#carregandoBairros').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')

    $('#gMedicos').attr('hidden', 'true')
    $.ajax({
        url: "php/buscaBairroNBen.php",
        method: 'Post',
        data: {
            cidade: cidade,
        },
        success: function (bairros) {
            bairros = JSON.parse(bairros)
            $('#bairros').html("<option disabled selected>Selecione</option>")
            bairros['BAIRRO'].forEach(bairro => {
                $('#bairros').append("<option>" + bairro.split(': ')[1] + "</option>")
            });
            $('#bairros').removeAttr('disabled')
            $('#carregandoBairros').attr('hidden', true)
            $('#buscaRealizada').removeAttr('hidden')
            setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)
        }
    })
}

function buscaClinicasNBen(cdPlano, especialidade, municipio, medicos, CRM, estabelecimento, uf, bairro) {
    $('#carregandoMedicos').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')

    $('#gMedicos').attr('hidden', 'true')

    var where = '';

    if (cdPlano.length > 0) {
        where += '"PLANO" LIKE ' + "'%" + cdPlano.split(' - ')[0] + "%'"
    }

    if (medicos.length > 0 && cdPlano.length == 0) {
        where += '"MEDICONOME" LIKE ' + "UPPER('%" + medicos + "%')"
    } else if (medicos.length > 0 && cdPlano.length > 0) {
        where += ' AND "MEDICONOME" LIKE ' + "UPPER('%" + medicos + "%')"
    }

    if (CRM.length > 0 && medicos.length == 0 && cdPlano.length == 0) {
        where += '"MedicoCRM" LIKE ' + "UPPER('%" + CRM + "%')"
    } else if (CRM.length > 0 && (medicos.length > 0 || cdPlano.length > 0)) {
        where += ' AND "MedicoCRM" LIKE ' + "UPPER('%" + CRM + "%')"
    }

    if (estabelecimento != null && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0) {
        where += '"MedicoLocalAtendimento" LIKE ' + "UPPER('%" + estabelecimento + "%')"
    } else if (estabelecimento != null && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0)) {
        where += ' AND "MedicoLocalAtendimento" LIKE ' + "UPPER('%" + estabelecimento + "%')"
    }

    if (especialidade != null && medicos.length == 0 && cdPlano.length == 0 && estabelecimento == null && CRM.length == 0) {
        where += 'vgma."ESPECIALIDADE" LIKE ' + "UPPER('%" + especialidade + "%')"
    } else if (especialidade != null && (medicos.length > 0 || cdPlano.length > 0 || estabelecimento != null || CRM.length > 0)) {
        where += ' AND vgma."ESPECIALIDADE" LIKE ' + "UPPER('%" + especialidade + "%')"
    }

    if (uf != null && especialidade == null && medicos.length == 0 && cdPlano.length == 0 && estabelecimento == null && CRM.length == 0) {
        where += '"UF" LIKE ' + "UPPER('%" + uf + "%')"
    } else if (uf != null && (medicos.length > 0 || cdPlano.length > 0 || especialidade != null || estabelecimento != null || CRM.length > 0)) {
        where += ' AND "UF" LIKE ' + "UPPER('%" + uf + "%')"
    }

    if (municipio.length > 0 && uf == null && especialidade == null && medicos.length == 0 && cdPlano.length == 0 && estabelecimento == null && CRM.length == 0) {
        where += '"CIDADE" LIKE ' + "UPPER('%" + municipio + "%')"
    } else if (municipio.length > 0 && (medicos.length > 0 || cdPlano.length > 0 || especialidade != null || uf != null || estabelecimento != null || CRM.length > 0)) {
        where += ' AND "CIDADE" LIKE ' + "UPPER('%" + municipio + "%')"
    }

    if (bairro != null && municipio.length == 0 && uf == null && especialidade == null && medicos.length == 0 && cdPlano.length == 0 && estabelecimento == null && CRM.length == 0) {
        where += '"BAIRRO" LIKE ' + "UPPER('%" + bairro + "%')"
    } else if (bairro != null && (medicos.length > 0 || cdPlano.length > 0 || especialidade != null || uf != null || municipio.length > 0 || estabelecimento != null || CRM.length > 0)) {
        where += ' AND "BAIRRO" LIKE ' + "UPPER('%" + bairro + "%')"
    }

    $.ajax({
        url: "php/buscaClinicasNBen.php",
        method: 'Post',
        data: {
            where: where
        },
        success: function (data) {
            guia_medico = JSON.parse(data)
            $('div#gMedicos').html('')

            guia_medico = handleGuiaMedico(guia_medico)

            if (guia_medico.length < 1) {
                alert('Nenhuma informação encontrada')
            } else {
                guia_medico.forEach(row => {
                    if (row.id != undefined) {
                        var id = row.id;
                        var bairro = row.bairro.split(':')[1]
                        var complemento = row.complemento[0].toUpperCase() + row.complemento.slice(1).toLowerCase()

                        newCard = document.createElement('div')
                        $(newCard).addClass('card')

                        newCardHeader = document.createElement('div')
                        $(newCardHeader).addClass('card-header text-center overflow-hidden')
                        $(newCardHeader).html(row.nome)

                        $(newCard).append(newCardHeader)

                        newCardBody = document.createElement('div')
                        $(newCardBody).addClass('card-body')
                        $(newCardBody).append('<strong>Tipo:</strong> ' + row.tipo + '<br>')

                        if (row.cnpj != undefined) {
                            $(newCardBody).append('<strong>CNPJ:</strong> ' + row.cnpj + '<br>')
                        }

                        $(newCardBody).append('<strong>Especialidade(s):</strong> ' + row.especialidade + '<br>')

                        if (complemento.length > 1){
                            $(newCardBody).append('<strong>Endereço: </strong>' + row.logradouro.replace(':', '') + ', ' + row.numero + ' - ' + complemento + ', ' + bairro + ' - ' + row.cidade + '/' + row.uf)
                        } else {
                            $(newCardBody).append('<strong>Endereço: </strong>' + row.logradouro.replace(':', '') + ', ' + row.numero + ', ' + bairro + ' - ' + row.cidade + '/' + row.uf)
                        }

                        $(newCardBody).append('<br><strong>Cep:</strong> ' + row.cep + '<br>')
                        $(newCardBody).append('<strong>Contato:</strong> ' + row.telefone + '<br><br>')

                        // if (row.certs.length > 0) {
                        //     titulos = document.createElement('div')
                        //     $(titulos).addClass('d-flex align-items-center')
                        //     $(titulos).append('Títulos:')

                        //     row.certs.forEach(cert => {
                        //         $(titulos).append(`<img style="height: 20px; margin-left: 8px;" src="${certs_img[cert - 1]}">`)
                        //     })

                        //     $(newCardBody).append(titulos)
                        // }

                        $(newCard).append(newCardBody)
                        $('#gMedicos').append(newCard)

                        $(newCardBody).append('<br><a href="https://www.google.com.br/maps/place/' + row.logradouro + '-' + row.cidade + '-' + bairro + '-' + row.numero + '" target="_blank"><button style="margin-left: 20%; margin-right: 1%" class="btn btn-primary mt-3 mb-3 float-right">Ver no Mapa</button></a>')
                        $(newCardBody).append('<a href="./infoAdicional.php?id=' + id + '" target="_blank"><button class="btn btn-primary mt-3 mb-3 float-right">Informações adicionais</button></a>')
                    }
                })

                $('#carregandoMedicos').attr('hidden', 'true')
                $('#buscaRealizada').removeAttr('hidden')
                $('#gMedicos').removeAttr('hidden')

                //legenda
                $('#legenda').html('')

                // for (i = 0; i < 16; i++) {
                //     cert = document.createElement('div')
                //     $(cert).addClass('col-6 d-flex align-items-center my-2')
                //     $(cert).append('<div style="width: 60px; margin-right: 16px;"></div>')
                //     $(cert).children('div').append(`<img src='${certs_img[i]}' height='24px;'>`)
                //     $(cert).append(certs_desc[i])

                //     $('#legenda').append(cert)
                // }

                setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)
            }
        }
    })
}