// const certs_img = [
//                 'img/APALC.png',
//                 'img/ADICQ.png',
//                 'img/AONA.png',
//                 'img/ACBA.png',
//                 'img/AIQG.png',
//                 'img/N.png',
//                 'img/P.png',
//                 'img/R.png',
//                 'img/E.png',
//                 'img/Q.png',
//                 'img/S.jfif',
//                 'img/PQ.jfif',
//                 'img/G.png',
//                 'img/I.png',
//                 'img/D.png',
//                 'img/M.png'
//             ]

// const certs_desc = [
//                 'Progr. acreditação de laboratórios',
//                 'Sistema nacional de acreditação',
//                 'Organização nacional de acreditação',
//                 'Consórcio nacional de acreditação',
//                 'Instituto qualisa gestão',
//                 'Comunicação de eventos adversos',
//                 'Profissional com especialização' ,
//                 'Profissional com residência',
//                 'Título de especialista',
//                 'Qualidade monitorada',
//                 'Segurança do paciente',
//                 'Proj. indução de qualidade',
//                 'Cert. entidades gestoras de outros programas',
//                 'Certificação ISO 9001',
//                 'Profissional com doutorado ou pós-doutorado',
//                 'Mestrado em saúde reconhecido pelo MEC'
//             ]

// const clinicas_santacasa = [
//     {
//         nome: 'Clínica Caraguá',
//         logradouro: 'Rua: Sao Jose Dos Campos',
//         numero: '25'
//     },
//     {
//         nome: 'Clínica Centro',
//         logradouro: 'Rua: Vilaca',
//         numero: '554'
//     },
//     {
//         nome: 'Clínica Jacareí',
//         logradouro: 'Rua: Floriano Peixoto',
//         numero: '281'
//     },
//     {
//         nome: 'Clínica Sul',
//         logradouro: 'Avenida: Andromeda',
//         numero: '1939'
//     },
//     {
//         nome: 'Clínica WK Diagnose',
//         logradouro: 'Rua: Major Jose Dos Santos Moreira',
//         numero: '305'
//     },
//     {
//         nome: 'Hospital Alvorada',
//         logradouro: 'Rua: Minas Gerais',
//         numero: '180'
//     },
//     {
//         nome: 'Hospital Antonio Afonso',
//         logradouro: 'Rua: Antonio Afonso',
//         numero: '307'
//     },
//     {
//         nome: 'Hospital FUSAM',
//         logradouro: 'Rua: Doutor Pereira De Mattos',
//         numero: '63'
//     },
//     {
//         nome: 'Hospital Regional Do Vale do Paraíba',
//         logradouro: 'Avenida: Tiradentes',
//         numero: '280'
//     },
//     {
//         nome: 'Hospital Santa Casa',
//         logradouro: 'Rua: Dolzani Ricardo',
//         numero: '620'
//     },
//     {
//         nome: 'Hospital Stella Maris',
//         logradouro: 'Avenida: Miguel Varlez',
//         numero: '980'
//     },
//     {
//         nome: 'Hospital de Clínicas de São Sebastião',
//         logradouro: 'Rua: Capitao Luiz Soares',
//         numero: '550'
//     },
//     {
//         nome: 'Instituto Santa Rosa',
//         logradouro: 'Rua: Dom Bosco',
//         numero: '378'
//     },
//     {
//         nome: 'Laboratório CEDLAB',
//         logradouro: 'Rua: Major Jose Dos Santos Moreira',
//         numero: '230'
//     },
//     {
//         nome: 'Laboratório Pró Imagem',
//         logradouro: 'Avenida: Nove De Julho',
//         numero: '216'
//     },
//     {
//         nome: 'Laboratório Sabin',
//         logradouro: 'Praça: Santa Terezinha',
//         numero: '43'
//     },
//     {
//         nome: 'Laboratório Vital Brasil',
//         logradouro: 'Rua: Nesralla Rubez',
//         numero: '366'
//     },
//     {
//         nome: 'Maternidade Santa Casa',
//         logradouro: 'Rua: Antonio Saes',
//         numero: '440'
//     },
//     {
//         nome: 'Nova Clínica Cruzeiro',
//         logradouro: 'Rua: Coronel Jose De Castro',
//         numero: '391'
//     },
//     {
//         nome: 'Pronto Atendimento',
//         logradouro: 'Rua: Vilaca',
//         numero: '843'
//     },
//     {
//         nome: 'Santa Casa Imagens Diagnósticas',
//         logradouro: 'Rua: Antonio Saes',
//         numero: '376'
//     },
//     {
//         nome: 'Santa Casa de Aparecida',
//         logradouro: 'Rua: Barao Do Rio Branco',
//         numero: '470'
//     },
//     {
//         nome: 'Santa Casa de Cruzeiro',
//         logradouro: 'Avenida: Major Novaes',
//         numero: '715'
//     },
//     {
//         nome: 'Santa Casa de Guararema',
//         logradouro: 'Rua: Doutor Botelho Egas',
//         numero: '11'
//     },
//     {
//         nome: 'Santa Casa de Guaratinguetá',
//         logradouro: 'Rua: Rangel Pestana',
//         numero: '194'
//     },
//     {
//         nome: 'Santa Casa de Lorena',
//         logradouro: 'Rua: Dom Bosco',
//         numero: '562'
//     },
//     {
//         nome: 'Santa Casa de Pindamonhangaba',
//         logradouro: 'Rua: Major Jose Dos Santos Moreira',
//         numero: '466'
//     }
// ]

function carregaEspecialidades(registro) {
    $('#erroIdentificacao').attr('hidden', 'true')
    $('#carregandoEspecialidades').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')
    
    $('#especialidades').attr('disabled', 'true')
    $('#especialidades').html("<option disabled selected>Selecione</option>")
    
    $('#municipios').attr('disabled', 'true')
    $('#municipios').html("<option disabled selected>Selecione</option>")
    
    $('#gMedicos').attr('hidden', 'true')
    $('#legenda').html('')
    
    $.ajax({
        url: "php/buscaEspecialidades.php",
        method: 'POST',
        data: {registro: registro},
        success: function(especialidades) {
            especialidades = JSON.parse(especialidades)
            if(especialidades['ESPECIALIDADE'].length > 0) {
                $('#especialidades').removeAttr('disabled')
                $('#carregandoEspecialidades').attr('hidden', 'true')
                $('#buscaRealizada').removeAttr('hidden')
                especialidades['ESPECIALIDADE'].forEach(especialidade => {
                    $('#especialidades').append("<option>" + especialidade + "</option>")
                });
                $('#cdPlano').attr('value', especialidades['CD_PLANO'][0])
                setTimeout(() => {$('#buscaRealizada').attr('hidden', 'true')}, 3000)
            } else {
                $('#erroIdentificacao').removeAttr('hidden')
                $('#carregandoEspecialidades').attr('hidden', 'true')
                $('#municipios').html("<option disabled selected>Selecione</option>")
                $('#especialidades').html("<option disabled selected>Selecione</option>")
            }
        }
    })
}

function carregaLocAtendimento(){
    $.ajax({
        url: "php/tipoLocalAtendimento.php",
        method: 'Post',
        success: function (tipoLocalAtendimento) {
            tipoLocalAtendimento = JSON.parse(tipoLocalAtendimento)
            $('select#estabelecimento').html('<option disabled selected>Selecione</option>')
            tipoLocalAtendimento['MedicoLocalAtendimento'].forEach(tipoLocalAtendimento => {
                $('select#estabelecimento').append('<option>' + tipoLocalAtendimento + '</option>')
            })
        }
    })
}

function buscaBairro(cidade) {
    $('#bairros').val('Selecione')
    $('#carregandoBairros').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')

    $('#gMedicos').attr('hidden', 'true')
    $.ajax({
        url: "php/buscaBairroNBen.php",
        method: 'Post',
        data: {
            cidade: cidade,
        },
        success: function (bairros) {
            bairros = JSON.parse(bairros)
            $('#bairros').html("<option disabled selected>Selecione</option>")
            bairros['BAIRRO'].forEach(bairro => {
                $('#bairros').append("<option>" + bairro.split(': ')[1] + "</option>")
            });
            $('#bairros').removeAttr('disabled')
            $('#carregandoBairros').attr('hidden', true)
            $('#buscaRealizada').removeAttr('hidden')
            setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)
        }
    })
}

function buscaMunicipios(cdPlano, especialidade) {
    $('#carregandoMunicipios').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')
    
    $('#especialidade').attr('value', especialidade)
    $('#municipios').attr('disabled', 'true')
    
    $('#gMedicos').attr('hidden', 'true')
    $('#legenda').html('')

    $.ajax({
        url: "php/buscaMunicipios.php",
        method: 'Post',
        data: {
            cdPlano: cdPlano,
            especialidade: especialidade
        },
        success: function(municipios) {
            municipios = JSON.parse(municipios)
            $('#municipios').html("<option disabled selected>Selecione</option>")
            municipios['CIDADE'].forEach(municipio => {
                $('#municipios').append("<option>" + municipio + "</option>")
            });
            $('#municipios').removeAttr('disabled')
            $('#carregandoMunicipios').attr('hidden', true)
            $('#buscaRealizada').removeAttr('hidden')
            setTimeout(() => {$('#buscaRealizada').attr('hidden', 'true')}, 3000)
        }
    })
}

function handleGuiaMedico(guia_medico) {
    let clinicas = []
    let medicos = []

    guia_medico.forEach(row => {
        // const clinica_found = clinicas_santacasa.find(clinica => {
        //     if(clinica.logradouro == row['Logradouro'] && clinica.numero == row['Numero']) {
        //         return true
        //     }
        //     return false
        // })

        // if(clinica_found != undefined) {
        //     const clinica_registered = clinicas.find(clinica => {
        //         if(clinica.nome == clinica_found.nome) {
        //             return true
        //         }
        //         return false
        //     })

        //     if(clinica_registered == undefined) {
        //         const new_clinica = {
        //             nome: clinica_found.nome,
        //             logradouro: row['Logradouro'],
        //             numero: row['Numero'],
        //             bairro: row['BAIRRO'],
        //             cep: row['CEP'],
        //             telefone: row['Telefone'],
        //             cidade: row['CIDADE'],
        //             certs: [],
        //             complemento: row['Complemento'],
        //             especialidade: row['ESPECIALIDADE'],
        //             uf: row['UF']
        //         }

        //         // if (row['SN_RESIDENCIA'] == 'B'){
        //         //     new_clinica.certs.push('8')
        //         // }if (row['SN_ISO_9001'] == 'S'){
        //         //     new_clinica.certs.push('14')
        //         // }if (row['SN_MESTRADO'] == 'S'){
        //         //     new_clinica.certs.push('16')
        //         // }

        //         clinicas.push(new_clinica)
        //     }

            
        // } else {
            const medico_registered = medicos.find(medico => {
                if(medico.id == row['MEDICOID']) {
                    return true
                }
                return false
            })

            var tipo = '';

            if(row['CNPJ'].length > 11){
                var cnpj = row['CNPJ'];
                tipo = 'Pessoa Jurídica';
            }else{
                tipo = 'Pessoa Física';
            }

            if(medico_registered == undefined) {
                const new_medico = {
                    id: row['MEDICOID'],
                    nome: row['MEDICONOME'],
                    logradouro: row['Logradouro'],
                    numero: row['Numero'],
                    bairro: row['BAIRRO'],
                    cep: row['CEP'],
                    telefone: row['Telefone'],
                    cidade: row['CIDADE'],
                    certs: [],
                    cnpj: cnpj,
                    tipo: tipo,
                    complemento: row['Complemento'],
                    especialidade: row['ESPECIALIDADE'],
                    uf: row['UF']
                }

                if (row['SN_RESIDENCIA'] == 'B'){
                    new_medico.certs.push('8')
                }if (row['SN_ISO_9001'] == 'S'){
                    new_medico.certs.push('14')
                }if (row['SN_MESTRADO'] == 'S'){
                    new_medico.certs.push('16')
                }

                // if(row['CERTIFICACAO'] != null) {
                //     new_medico.certs.push(row['CERTIFICACAO'])
                // }

                medicos.push(new_medico)
            } 
            
            // else {
            //     const medico_pos = medicos.indexOf(medico_registered)
            //     medicos[medico_pos].certs.push(row['CERTIFICACAO'])
            // }
        // }

    })
    
    return guia_medico = clinicas.concat(medicos)
}

function buscaClinicas(cdPlano, medicos, CRM, estabelecimento, especialidade, uf, municipio, bairro) {
    $('#carregandoMedicos').removeAttr('hidden')
    $('#buscaRealizada').attr('hidden', 'true')
    
    if (cdPlano.length == 0) {
        alert('Por favor, informe seu CPF ou Carteirinha')
    }else{

    var where = '';

    if (cdPlano.length > 0) {
        where += '"CODIGOPLANO" = ' + cdPlano
    }

    if (medicos.length > 0 && cdPlano.length == 0) {
        where += '"MEDICONOME" LIKE ' + "UPPER('%" + medicos + "%')"
    } else if (medicos.length > 0 && cdPlano.length > 0) {
        where += ' AND "MEDICONOME" LIKE ' + "UPPER('%" + medicos + "%')"
    }

    if (CRM.length > 0 && medicos.length == 0 && cdPlano.length == 0) {
        where += '"MedicoCRM" LIKE ' + "UPPER('%" + CRM + "%')"
    } else if (CRM.length > 0 && (medicos.length > 0 || cdPlano.length > 0)) {
        where += ' AND "MedicoCRM" LIKE ' + "UPPER('%" + CRM + "%')"
    }

    if (estabelecimento != null && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0) {
        where += '"MedicoLocalAtendimento" LIKE ' + "UPPER('%" + estabelecimento + "%')"
    } else if (estabelecimento != null && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0)) {
        where += ' AND "MedicoLocalAtendimento" LIKE ' + "UPPER('%" + estabelecimento + "%')"
    }

    if (especialidade.length > 0 && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0 && estabelecimento == null) {
        where += 'vgma."ESPECIALIDADE" LIKE ' + "UPPER('%" + especialidade + "%')"
    } else if (especialidade.length > 0 && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0 || estabelecimento != null)) {
        where += ' AND vgma."ESPECIALIDADE" LIKE ' + "UPPER('%" + especialidade + "%')"
    }

    if (uf != null && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0 && estabelecimento == null && especialidade.length == 0) {
        where += '"UF" LIKE ' + "UPPER('%" + uf + "%')"
    } else if (uf != null && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0 || estabelecimento != null || especialidade.length > 0)) {
        where += ' AND "UF" LIKE ' + "UPPER('%" + uf + "%')"
    }

    if (municipio.length > 0 && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0 && estabelecimento == null && especialidade.length == 0 && uf == null) {
        where += '"CIDADE" LIKE ' + "UPPER('%" + municipio + "%')"
    } else if (municipio.length > 0 && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0 || estabelecimento != null || especialidade.length > 0 || uf != null)) {
        where += ' AND "CIDADE" LIKE ' + "UPPER('%" + municipio + "%')"
    }

    if (bairro != null && medicos.length == 0 && cdPlano.length == 0 && CRM.length == 0 && estabelecimento == null && especialidade.length == 0 && uf == null && municipio.length == 0) {
        where += '"BAIRRO" LIKE ' + "UPPER('%" + bairro + "%')"
    } else if (bairro != null && (medicos.length > 0 || cdPlano.length > 0 || CRM.length > 0 || estabelecimento != null || especialidade.length > 0 || uf != null || municipio.length > 0)) {
        where += ' AND "BAIRRO" LIKE ' + "UPPER('%" + bairro + "%')"
    }

    $('#gMedicos').attr('hidden', 'true')
    $.ajax({
        url: "php/buscaClinicas.php",
        method: 'Post',
        data: {
            where: where
        },
        success: function (data) {
            guia_medico = JSON.parse(data)
            $('div#gMedicos').html('')

            guia_medico = handleGuiaMedico(guia_medico)

            if (guia_medico.length < 1) {
                alert('Nenhuma informação encontrada')
            } else {
                guia_medico.forEach(row => {
                    if (row.id != undefined) {
                        var id = row.id;
                        var bairro = row.bairro.split(':')[1]
                        var complemento = row.complemento[0].toUpperCase() + row.complemento.slice(1).toLowerCase()

                        newCard = document.createElement('div')
                        $(newCard).addClass('card')

                        newCardHeader = document.createElement('div')
                        $(newCardHeader).addClass('card-header text-center overflow-hidden')
                        $(newCardHeader).html(row.nome)

                        $(newCard).append(newCardHeader)

                        newCardBody = document.createElement('div')
                        $(newCardBody).addClass('card-body')
                        $(newCardBody).append('<strong>Tipo:</strong> ' + row.tipo + '<br>')

                        if (row.cnpj != undefined) {
                            $(newCardBody).append('<strong>CNPJ:</strong> ' + row.cnpj + '<br>')
                        }

                        $(newCardBody).append('<strong>Especialidade(s):</strong> ' + row.especialidade + '<br>')

                        if (complemento.length > 1){
                            $(newCardBody).append('<strong>Endereço: </strong>' + row.logradouro.replace(':', '') + ', ' + row.numero + ' - ' + complemento + ', ' + bairro + ' - ' + row.cidade + '/' + row.uf)
                        } else {
                            $(newCardBody).append('<strong>Endereço: </strong>' + row.logradouro.replace(':', '') + ', ' + row.numero + ', ' + bairro + ' - ' + row.cidade + '/' + row.uf)
                        }

                        $(newCardBody).append('<br><strong>Cep:</strong> ' + row.cep + '<br>')
                        $(newCardBody).append('<strong>Contato:</strong> ' + row.telefone + '<br><br>')

                        // if (row.certs.length > 0) {
                        //     titulos = document.createElement('div')
                        //     $(titulos).addClass('d-flex align-items-center')
                        //     $(titulos).append('Títulos:')

                        //     row.certs.forEach(cert => {
                        //         $(titulos).append(`<img style="height: 20px; margin-left: 8px;" src="${certs_img[cert - 1]}">`)
                        //     })

                        //     $(newCardBody).append(titulos)
                        // }

                        $(newCard).append(newCardBody)
                        $('#gMedicos').append(newCard)

                        $(newCardBody).append('<br><a href="https://www.google.com.br/maps/place/' + row.logradouro + '-' + row.cidade + '-' + bairro + '-' + row.numero + '" target="_blank"><button style="margin-left: 20%; margin-right: 1%" class="btn btn-primary mt-3 mb-3 float-right">Ver no Mapa</button></a>')
                        $(newCardBody).append('<a href="./infoAdicional.php?id=' + id + '" target="_blank"><button class="btn btn-primary mt-3 mb-3 float-right">Informações adicionais</button></a>')
                    }
                })

                $('#carregandoMedicos').attr('hidden', 'true')
                $('#buscaRealizada').removeAttr('hidden')
                $('#gMedicos').removeAttr('hidden')

                //legenda
                $('#legenda').html('')

                // for (i = 0; i < 16; i++) {
                //     cert = document.createElement('div')
                //     $(cert).addClass('col-6 d-flex align-items-center my-2')
                //     $(cert).append('<div style="width: 60px; margin-right: 16px;"></div>')
                //     $(cert).children('div').append(`<img src='${certs_img[i]}' height='24px;'>`)
                //     $(cert).append(certs_desc[i])

                //     $('#legenda').append(cert)
                // }

                setTimeout(() => { $('#buscaRealizada').attr('hidden', 'true') }, 3000)
            }
        }
    })
    }
}