<?php include 'html/header.html'; ?>
<form  class="form" id="fm" action="buscaMedicos.php" method="POST"> 
    <div class="d-flex justify-content-between">
        <label id="campo">CPF ou Carteirinha</label>  
        <a href="naoBeneficiario.php">Não sou beneficiário</a>
    </div>   
    <input id="registro" class="form-control mb-3" type="number" onchange="carregaEspecialidades($(this).val())">

    <label>Nome do Médico ou Prestador</label>
    <input type="text" class="form-control mb-3" name="medico" id="medico" placeholder="Digite o nome do médico ou prestador" required>

    <label>CRM</label>
    <input type="text" class="form-control mb-3" name="CRM" id="CRM" placeholder="Digite o CRM" required>
      
    <label>Selecione o tipo de Estabelecimento</label>
    <select name="estabelecimento" id="estabelecimento" class="form-control mb-3" onchange="$('#estabelecimento').val().toUpperCase()">
        <option disabled selected>Selecione</option>
    </select>

    <label>Selecione a especialidade</label>
    <select disabled name="especialidades" id="especialidades" class="form-control mb-3" onchange="buscaMunicipios($('#cdPlano').val(), $('#especialidades').val().toUpperCase())">
        <option disabled selected>Selecione</option>
    </select>

    <label>UF</label>    
    <select name="UF" id="UF" class="form-control mb-3">
        <option disabled selected>Selecione</option>
    </select>

    <label>Selecione seu município</label>    
    <select disabled name="municipios" id="municipios" class="form-control mb-3" onchange="$('#municipio').attr('value', $(this).val().toUpperCase(), buscaBairro($(this).val()))">
    <option disabled selected>Selecione</option>
    </select>

    <label>Bairro</label>    
    <select disabled name="bairros" id="bairros" class="form-control mb-3" onchange="$('#bairros').attr('value', $(this).val().toUpperCase())">
        <option disabled selected>Selecione</option>
    </select>

    <input hidden id="cdPlano" name='cdPlano' value="">
    <input hidden id="municipio" name='municipio' value="">
    <input hidden id="especialidade" name='especialidade' value="">   
</form>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
<div class="d-flex align-items-center">
    <p id='erroIdentificacao' hidden class="flex-fill text-right text-danger">Identificação inválida!</p>
    <p id='carregandoEspecialidades' hidden class="flex-fill text-right text-warning">Buscando especialidades disponíveis</p>
    <p id='carregandoMunicipios' hidden class="flex-fill text-right text-warning">Buscando municípios para atendimento</p>
    <p id='carregandoMedicos' hidden class="flex-fill text-right text-warning">Buscando médicos e clínicas</p>
    <p id='buscaRealizada' hidden class="flex-fill text-right text-success">Busca relizada com sucesso</p>
</div>

<div>
    <a class="btn btn-primary mt-3 mb-5" onclick="buscaClinicas($('#cdPlano').val(), $('#medico').val(), $('#CRM').val(), $('#estabelecimento').val(), $('#especialidade').val(), $('#UF').val(), $('#municipio').val(), $('#bairros').val())">Buscar</a>
    <a class="btn btn-warning mt-3 mb-5 float-right" onclick="limpaCampos()">Limpar Campos</a>
</div>

</div>
    <div id="gMedicos" class="m-5 w-75"></div>
    <div id="legenda" class="m-5 w-75 row"></div>
    <script>carregaUF()</script>
    <script>carregaLocAtendimento()</script>

<script>
    function limpaCampos(){
        $('#bairros').attr("disabled", true);
        $('#municipios').attr("disabled", true);
        $('#especialidades').attr("disabled", true);
        $("#fm").trigger('reset')
    }
</script>
<?php include 'html/footer.html';?>