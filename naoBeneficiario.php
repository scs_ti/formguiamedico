<?php include 'html/header.html'; ?>
<form  class="form" id="fm" action="buscaMedicos.php" method="POST">    
    
    <div class="d-flex justify-content-between">
        <label id="campo">Selecione o plano</label>  
        <a href="beneficiario.php">Sou beneficiário</a>
    </div>  

    <select name="plano" id="plano" class="form-control mb-3" onchange="carregaEspecialidadesNBen($(this).val().toUpperCase())">
        <option disabled selected>PLANO</option>
    </select>

    <label>Nome do Médico ou Prestador</label>
    <input type="text" class="form-control mb-3" name="medico" id="medico" placeholder="Digite o nome do médico ou prestador" required>

    <label>CRM</label>
    <input type="text" class="form-control mb-3" name="CRM" id="CRM" placeholder="Digite o CRM" required>

    <label>Selecione o tipo de Estabelecimento</label>
    <select name="estabelecimento" id="estabelecimento" class="form-control mb-3" onchange="$('#estabelecimento').val().toUpperCase()">
        <option disabled selected>Selecione</option>
    </select>

    <label>Selecione a especialidade</label>
    <select name="especialidades" disabled id="especialidades" class="form-control mb-3" onchange="$('#especialidades').val().toUpperCase()">
        <option disabled selected>Selecione</option>
    </select>

    <label>UF</label>    
    <select name="UF" id="UF" onchange="buscaMunicipiosNBen($(this).val())" class="form-control mb-3">
        <option disabled selected>Selecione</option>
    </select>

    <label>Municipios</label>    
    <select disabled name="municipios" id="municipios" class="form-control mb-3" onchange="$('#municipio').attr('value', $(this).val().toUpperCase(), buscaBairro($(this).val()))">
        <option disabled selected>Selecione</option>
    </select>

    <label>Bairro</label>    
    <select disabled name="bairros" id="bairros" class="form-control mb-3" onchange="$('#bairros').attr('value', $(this).val().toUpperCase())">
        <option disabled selected>Selecione</option>
    </select>

    <input hidden id="cdPlano" name='cdPlano' value="">
    <input hidden id="municipio" name='municipio' value="">
    <input hidden id="especialidade" name='especialidade' value="">   
    
</form>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
<div class="d-flex align-items-center">
    <p id='erroIdentificacao' hidden class="flex-fill text-right text-danger">Identificação inválida!</p>
    <p id='carregandoEspecialidades' hidden class="flex-fill text-right text-warning">Buscando especialidades disponíveis</p>
    <p id='carregandoUFs' hidden class="flex-fill text-right text-warning">Buscando Estados para atendimento</p>
    <p id='carregandoMunicipios' hidden class="flex-fill text-right text-warning">Buscando Municípios para atendimento</p>
    <p id='carregandoBairros' hidden class="flex-fill text-right text-warning">Buscando Bairros para atendimento</p>
    <p id='carregandoMedicos' hidden class="flex-fill text-right text-warning">Buscando médicos e clínicas</p>
    <p id='buscaRealizada' hidden class="flex-fill text-right text-success">Busca relizada com sucesso</p>
</div>

<div>
    <a class="btn btn-primary mt-3 mb-5" onclick="buscaClinicasNBen($('#cdPlano').val(), $('#especialidades').val(), $('#municipio').val(), $('#medico').val(), $('#CRM').val(), $('#estabelecimento').val(), $('#UF').val(), $('#bairros').val())">Buscar</a>
    <a class="btn btn-warning mt-3 mb-5 float-right" onclick="limpaCampos()">Limpar Campos</a>
</div>

</div>
    <div id="gMedicos" class="m-5 w-75"></div>
    <div id="legenda" class="m-5 w-75 row"></div>
    <script>carregaPlanos()</script>
    <script>carregaUF()</script>
    <script>carregaLocAtendimento()</script>

<script>
    function limpaCampos(){
        $('#bairros').attr("disabled", true);
        $('#municipio').attr("disabled", true);
        $("#fm").trigger('reset')
    }
</script>
<?php include 'html/footer.html';?>