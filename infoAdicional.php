<?php 
    include 'html/header2.html'; 
    include 'php/connect.php';  

    $conexao = DBOps::connect();
    $id = $_GET['id'];
    $x = '';
?>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>

<div>
  <div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
                $sql     = 'SELECT distinct INITCAP(MEDICONOME) AS MEDICONOME, "MedicoCRM", CNPJ FROM v_guia_medico_app WHERE MEDICOID = '.$_GET['id'].' ';
                $sql_esp = 'SELECT distinct ESPECIALIDADE FROM v_guia_medico_app WHERE MEDICOID = '.$_GET['id'].' ';
                $sql_end = 'SELECT distinct INITCAP("MedicoLocalAtendimento") AS "MedicoLocalAtendimento", INITCAP("Logradouro") AS "logradouro", "Numero", INITCAP("Complemento") as "Complemento", INITCAP(BAIRRO) AS "BAIRRO", INITCAP(CIDADE) AS "CIDADE", UF, CEP, "Telefone", "Email" FROM v_guia_medico_app WHERE MEDICOID = '.$_GET['id'].' ';
                $sql_planos = 'SELECT distinct vg.CODIGOPLANO, pg.RG_ANS, pg.TIPO_CONTRATO, pg.ENVIA_ANS, vg.PLANO, vg."Classificao_Guia" FROM v_guia_medico_app vg
                INNER JOIN planos_guia_medico pg ON pg.COD_PLANO = vg.CODIGOPLANO 
                WHERE MEDICOID = '.$_GET['id'].'';
                $sql_info_add = 'SELECT distinct CNPJ_CPF, NOME_FANTASIA, INITCAP("NM_PRESTADOR") AS "RAZAO_SOCIAL", EMAIL, TIPO FROM base_dados_guia_medico bd INNER JOIN v_guia_medico_app vg ON (vg.MEDICONOME = bd.NOME_FANTASIA OR vg.MEDICONOME = bd.NM_PRESTADOR) WHERE vg.MEDICOID =  '.$_GET['id'].'';
                
                $stid  = oci_parse($conexao, $sql) or die ("erro");
                $stid2 = oci_parse($conexao, $sql_esp) or die ("erro");
                $stid3 = oci_parse($conexao, $sql_end) or die ("erro");
                $stid4 = oci_parse($conexao, $sql_planos) or die ("erro");
                $stid5 = oci_parse($conexao, $sql_info_add) or die ("erro");
                
                oci_execute($stid);
                oci_execute($stid2); 
                oci_execute($stid3); 
                oci_execute($stid4);
                oci_execute($stid5);

                while (oci_fetch($stid)) {
                    echo '<div class="w-100 mt-5">
                            <center><h2>'.oci_result($stid, "MEDICONOME").'</h2></center>
                         </div>';  
                    
                    if (strlen(oci_result($stid, "MedicoCRM")) > 1){
                        echo '<p><strong>CRM:</strong> '.oci_result($stid, "MedicoCRM").'</strong></p>';
                    }
                    
                    if (strlen(oci_result($stid, "CNPJ")) > 12) {
                        $cnpj = oci_result($stid, "CNPJ");
                        echo '<p><strong>Tipo: </strong>Pessoa Jurídica</p>
                              <p><strong>CNPJ:</strong> '.oci_result($stid, "CNPJ").'</strong></p>';
                    } else {
                        echo '<p><strong>Tipo: </strong>Pessoa Fisíca</p>';
                    }

                    
                }
                
                while (oci_fetch($stid5)){
                    echo '<p><strong>Razão Social:</strong> '.oci_result($stid5, "RAZAO_SOCIAL").'</strong></p>';
                    $email = oci_result($stid5, "EMAIL");
                }

                while (oci_fetch($stid2)) {
                    $x .= ucfirst(strtolower(oci_result($stid2, "ESPECIALIDADE"))).', ';
                }

                echo '<p><strong>Especialidade(s) ou serviços contratados: </strong>'.substr($x, 0, -2).'</p>
                      <hr data-uw-styling-context="true"> <p><h2>Rede Basica</h2></p>';
                
                while (oci_fetch($stid3)){
                    $bairro = explode(':', oci_result($stid3, "BAIRRO"))[1];
                    echo "
                    <p><strong>Tipo de estabelecimento: </strong>".oci_result($stid3, "MedicoLocalAtendimento")."</p>
                    <p><strong>Endereço: </strong>".str_replace(':', '', oci_result($stid3, "logradouro")).", ".oci_result($stid3, "Numero"). ", " .oci_result($stid3, "Complemento"). " - " .$bairro.", ".oci_result($stid3, "CIDADE")."/".oci_result($stid3, "UF").", ".oci_result($stid3, "CEP")."
                    <a href='https://www.google.com.br/maps/place/".str_replace(':', '', oci_result($stid3, "logradouro"))."-".oci_result($stid3, "CIDADE")."-".$bairro."-".oci_result($stid3, "Numero")."' target='_blank'>(Veja no Mapa)</a></p>
                    <p><strong>Telefone: </strong>".oci_result($stid3, "Telefone")."</p>";
                }

                if (isset($email)){
                    echo '<p><strong>E-mail: </strong>'.$email.'</p>';
                }
                
                echo "<hr data-uw-styling-context='true'><p><h2>Planos Atendidos</h2></p>";

                while (oci_fetch($stid4)){
                    if (oci_result($stid4, "ENVIA_ANS") == 'S'){
                    echo "<p><strong>".oci_result($stid4, "PLANO")."</strong><p>
                    <p><strong>Registro ANS: </strong>".oci_result($stid4, "RG_ANS")."</p>
                    <p><strong>Tipo de contratação: </strong>".oci_result($stid4, "TIPO_CONTRATO")."</p>
                    <p><strong>Situação junto à ANS: </strong>Ativo</p><br>";
                    }
                }
            ?>
        </div>
    </div>
  </div>
</div>

<?php include 'html/footer.html';?>